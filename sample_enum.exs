defmodule Sample.Enum do
    def first(list) when length(list) == 0, do: nil

    def first([_ | tail]) do
        tail
    end

    def add(list, val \\ 0) do
        [val | list]
    end
end