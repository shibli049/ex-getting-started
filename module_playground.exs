defmodule ModulePlayground do
    import IO, only: [puts: 1]
    import Kernel, except: [inspect: 1]

    alias ModulePlayground.Misc.Util.Math, as: MyMath

    require Integer

    def sayHere do
        inspect("Here I am!!")
    end
    
    def inspect(param1) do
        puts "starting"
        puts param1
        puts "ending"
    end

    def printSum(a, b) do
        MyMath.add(a, b)
    end

    def print_is_even(num) do
        puts "Is #{num} even? #{Integer.is_even(num)}"
    end
    
end